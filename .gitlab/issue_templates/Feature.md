## Feature Request

#### Is your feature request related to a problem? Please describe.
A clear and concise description of what the problem is. Ex. I have an issue when [...]

#### Solution
A clear and concise description of what you want to happen. Add any considered drawbacks.

#### Alternatives you've considered
A clear and concise description of any alternative solutions or features you've considered.

#### Teachability, Documentation, Adoption, Migration Strategy
If you can, explain how users will be able to use this and possibly write out a version of the docs.
Maybe a screenshot or design?