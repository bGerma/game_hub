"""
this is the hangman game
"""
import random

from game_hub.utils.logs import logger
from game_hub.utils.file.path import data_dir
from game_hub.utils.input.inputValidators import StringValidator
from game_hub.utils.input.getInput import get_input

logger = logger.logging.getLogger('gamehub.games.hangMan')

default_input_config = {
    'guess': {'mode': 'prompt', 'prompt_desc': {
        'type': 'input',
        'message': 'Type your guess:',
        'name': 'hangman_guess',
        'validate': StringValidator}
    }
}


def get_random_word(words_path=None) -> str:
    """
    get_random_word() gets a random word by opening a file with each word separated by a new line. It then splits the
    file into a list of the words. Finally, it picks a random one by passing in the list of words to random.choice

    :param words_path: a file path to a text file containing all the words
    :return: a random word
    """
    words_path = words_path if words_path is not None else data_dir / 'hangManWords.txt'
    with open(words_path, 'r') as hang_man_words:
        words = hang_man_words.read().split()
    return random.choice(words)


def guess_is_correct(guess: chr or str, word: str, hidden_chars: list) -> bool:
    """
    guess_is_correct() loops through the chars in the word and checks to see if guess is in it. If guess is equal to
    that char, it replaces the underscore where that char would be in hidden_chars for the guess.

    :param hidden_chars: a list of the word but all the chars are underscores
    :param guess: a guessed letter or word that the user thinks is in or is the word
    :param word: the word that the user has to try to guess
    :returns: was the guess correct
    """
    is_guess_correct = False
    if guess == word:
        return True
    for i in range(word.__len__()):
        if guess == word[i]:
            is_guess_correct = True
            hidden_chars[i] = guess
    logger.info(' '.join(hidden_chars))
    return is_guess_correct


def hang_man_game(word: str, input_config: dict = None) -> str:
    """
    hang_man_game() is the function that plays the game. Fist, it makes hidden_chars by multiplying an underscore by
    the length of the word. Then in a while loop, it first gets the the user input using the input_config to specify the
    type of input then if the guess is the entire word or if the user had guessed all the individual letters,
    the function returns 'Congrats, you won!'. However, if you ran out of chances, hang_man_game() returns 'you
    failed, the word was {word}'. But, if both of these if statements are unsatisfied it continues with the game and
    checks if the guess is in the word, and if it's not in the word hang_man_game() takes away a chance and prints how
    many lives you have left. It continues until one of the previous if statements are satisfied.

    :param input_config: the input configuration dictionary that will passed into get_input()
    :param word: the word that the user has to guess
    :returns: a message reporting if you had won or lost
    """
    hidden_chars = ['_'] * len(word)
    logger.info("the hang man game has been picked")
    logger.info(' '.join(hidden_chars))
    input_config = input_config if input_config is not None else default_input_config
    chances = 10
    while True:
        guess = get_input(input_config['guess'])

        if guess_is_correct(guess, word, hidden_chars) is False:
            chances -= 1
            logger.info(f"incorrect!\nYou have {str(chances)} lives/life left")

        if guess == word or list(word) == hidden_chars:
            return "Congrats, you won!"
        elif chances == 0:
            return f"You failed,\nThe word was {word}"


def game() -> None:  # pragma: no cover
    """
    plays the hangman game
    """
    end_message = hang_man_game(get_random_word())
    logger.info(end_message)


if __name__ == '__main__':  # pragma: no cover
    game()
