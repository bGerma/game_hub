"""
Games cli
"""
import click
import json
from pathlib import Path

from game_hub.games import hangMan, ticTacToe, rockPaperScissors
from game_hub.utils.file.jsonUtils import load_json_path
from game_hub.utils.file.path import configs


gamehub_config_path = configs / 'gameHub.json'


def change_last_game(last_game: str, last_game_config_path: Path) -> None:
    """
    changes the value of the last game key to the last_game param

    :param last_game_config_path: the configuration file that stores the last_game
    :param last_game: the
    """
    gamehub_config = load_json_path(last_game_config_path, validate_json=False)
    gamehub_config['last_game'] = last_game
    with last_game_config_path.open(mode='w') as json_output:
        json.dump(gamehub_config, json_output, indent=2)


@click.group('play', short_help='group of all games')
def play() -> None:
    """
    Contains all games
    """
    pass


@play.command('hang', short_help='Plays Hangman game')
def hangman() -> None:
    """
    Plays Hangman game
    """
    hangMan.game()
    change_last_game('hangman', gamehub_config_path)


@play.command('rps', short_help='Plays Rock Paper Scissors game')
def rps() -> None:
    """
    Plays rock paper scissors game
    """
    rockPaperScissors.game()
    change_last_game('rps', gamehub_config_path)


@play.command('tic', short_help='Plays Tic Tac Toe game')
def tictactoe() -> None:
    """
    Plays Tic tac toe game
    """
    ticTacToe.game()
    change_last_game('tictactoe', gamehub_config_path)
