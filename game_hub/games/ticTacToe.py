"""
this is the tic tac toe game
"""
import json
import random
import shutil
import regex
import filecmp

from pathlib import Path
from colorama import init
from prompt_toolkit.validation import Validator, ValidationError

from game_hub.utils.file.path import data_dir, configs
from game_hub.utils.file.jsonUtils import load_json_path
from game_hub.utils.logs.logger import logging
from game_hub.utils.input.inputValidators import StringValidator
from game_hub.utils.input.getInput import get_input

init(autoreset=True)
logger = logging.getLogger('gamehub.games.ticTacToe')

tutorial_file_path = data_dir / 'tutorial.txt'
default_tic_config_path = data_dir / 'default_ticTacToe.json'
tic_config_path = configs / 'games/ticTacToe.json'
if tic_config_path.exists() is False:
    tic_config_path.parent.mkdir(parents=True, exist_ok=True)
    shutil.copy(default_tic_config_path, tic_config_path)
elif filecmp.cmp(default_tic_config_path, tic_config_path):
    shutil.copy(default_tic_config_path, tic_config_path)
tic_config = load_json_path(tic_config_path)

evaluation_to_message = {
    "player 1": "player 1 won",
    "player 2": "player 2 won",
    "ai": "ai won",
    "tie": "It is a draw",
    "human": "you won"
}
potential_scores = {
    "player 1": 1,
    "tie": 0,
    "player 2": -1
}
switch_player = {
    "player 1": "player 2",
    "player 2": "player 1"
}
board_keys = [
    "top_left", "top_mid", "top_right",
    "mid_left", "mid_mid", "mid_right",
    "bot_left", "bot_mid", "bot_right"
]


class IntValidator(Validator):
    """
    thing
    """

    def validate(self, document) -> None:
        """
        validates the input
        """

        not_ok = regex.search('[^0-9]', document.text)
        if not_ok:
            raise ValidationError(
                message='Please enter a int',
                cursor_position=len(document.text)
            )


class OneToNineValidator(Validator):
    """
    thing
    """

    def validate(self, document) -> None:
        """
        validates the input
        """
        has_already_been_picked = False
        picked_slots = tic_config['picked_slots']
        for i in range(picked_slots.__len__()):
            if str(picked_slots[i]) in document.text:
                has_already_been_picked = True
        is_not_from_1_to_9 = not regex.match('^[0-9]{1}$', document.text)
        if is_not_from_1_to_9 or has_already_been_picked:
            raise ValidationError(
                message="Please enter a int from one to nine that hasn't been picked yet",
                cursor_position=len(document.text)
            )


default_input_config = {
    'tutorial': {'mode': 'prompt', 'prompt_desc': {
        'type': 'confirm',
        'message': 'Do you want to see the tutorial?',
        'name': 'tutorial',
        'default': True
    }},
    'mode': {'mode': 'prompt', 'prompt_desc': {
        'type': 'list',
        'name': 'mode',
        'message': 'What mode do you want?',
        'choices': [ai_mode['Name'] for ai_mode in tic_config['ai_modes']] + ['Custom'],
    }},
    'player_going_first': {
        'mode': 'pass'
    },
    'mode_name': {'mode': 'prompt', 'prompt_desc': {
        'type': 'input',
        'name': 'mode_name',
        'message': 'name of custom mode:',
        'validate': StringValidator
    }},
    'freq_lose': {'mode': 'prompt', 'prompt_desc': {
        'type': 'input',
        'name': 'freq_lose',
        'message': 'frequency of losing moves :',
        'validate': IntValidator
    }},
    'freq_draw': {'mode': 'prompt', 'prompt_desc': {
        'type': 'input',
        'name': 'freq_draw',
        'message': 'frequency of drawing moves:',
        'validate': IntValidator
    }},
    'freq_win': {'mode': 'prompt', 'prompt_desc': {
        'type': 'input',
        'name': 'freq_win',
        'message': 'frequency of winning moves:',
        'validate': IntValidator
    }},
    'confirm_choices': {'mode': 'prompt', 'prompt_desc': {
        'type': 'confirm',
        'name': 'confirm_choices',
        'message': 'Are you sure about these choices?',
    }},
    'human_or_ai': {'mode': 'prompt', 'prompt_desc': {
        'type': 'confirm',
        'name': 'human_or_ai',
        'message': 'Do you want to play against the AI?',
    }},
    'picked_slot': {'mode': 'prompt', 'prompt_desc': {
        'type': 'input',
        'name': 'picked_slot',
        'message': 'Pick a slot:',
        'validate': OneToNineValidator
    }},
    'ai_pick': {
        'mode': 'pass'
    },
    'ai_player': {
        'mode': 'pass'
    }
}


def pick_ai_move(board: dict, current_player: str, ai_mode: str) -> int:
    """
    using the mini_max function, it loops through all the moves for this turn and then finds out the best one by calling
    mini_max, and dependant on who's turn it is, the computer either saves the best score by looking for the lowest
    score (meaning player two's best move) or the highest score (meaning player one's best move)
    :param ai_mode: The mode of the ai
    :param board: the tic tac toe board
    :param current_player: player who has the turn
    """
    if current_player == "player 1":
        maximising_player = False
        symbol_to_put_in_slot = "x"
    else:
        maximising_player = True
        symbol_to_put_in_slot = "o"
    best_move = float("-inf")
    mini_best_score = float("-inf")
    max_best_score = float("inf")
    if all(x == "-" for x in board.values()):
        return random.randint(0, 8)
    while best_move == float("-inf"):
        for i in range(9):
            if board[board_keys[i]] == '-':
                board[board_keys[i]] = symbol_to_put_in_slot
                score = mini_max(board, maximising_player)
                if ai_mode == "win":
                    if score > mini_best_score and not maximising_player:
                        mini_best_score = score
                        best_move = i
                    if score < max_best_score and maximising_player:
                        max_best_score = score
                        best_move = i
                if ai_mode == "draw":
                    if score == 0 and not maximising_player:
                        mini_best_score = score
                        best_move = i
                    if score == 0 and maximising_player:
                        max_best_score = score
                        best_move = i
                if ai_mode == "lose":
                    if score == -1 and not maximising_player:
                        mini_best_score = score
                        best_move = i
                    if score == 1 and maximising_player:
                        mini_best_score = score
                        best_move = i
                board[board_keys[i]] = "-"
        if ai_mode == "draw":
            ai_mode = "win"
        if ai_mode == "lose":
            ai_mode = "draw"
    return best_move


def evaluate_board(board: dict, ai_player: str = None) -> str:
    """
    takes in a board as a dictionary and a list and then uses them to check who is the winner, loser or if it is a tie
    or the game hasn't finished yet.
    :param ai_player: if the user is playing against the ai, which player is the ai
    :param board: dictionary of all slots on the game board
    :return: the winning player, a tie or an empty string if the game is not finished
    """
    winner = ""
    slots = list(board.values())

    for symbol in ["x", "o"]:
        row_1 = slots[0] == slots[1] == slots[2] == symbol
        row_2 = slots[3] == slots[4] == slots[5] == symbol
        row_3 = slots[6] == slots[7] == slots[8] == symbol

        col_1 = slots[0] == slots[3] == slots[6] == symbol
        col_2 = slots[1] == slots[4] == slots[7] == symbol
        col_3 = slots[2] == slots[5] == slots[8] == symbol

        dia_1 = slots[0] == slots[4] == slots[8] == symbol
        dia_2 = slots[2] == slots[4] == slots[6] == symbol
        if row_1 or row_2 or row_3 or col_1 or col_2 or col_3 or dia_1 or dia_2:
            winner = {'x': 'player 1', 'o': 'player 2', }[symbol]
            break
    if winner == "":
        for value in board.values():
            if value == "-":
                return ""
        return "tie"
    if ai_player:
        human_player = switch_player[ai_player]
        if ai_player == winner:
            winner = "ai"
        if human_player == winner:
            winner = "human"
    return winner


def mini_max(board: dict, maximizing_player: bool) -> int:
    """
    Scoring system: 1 -> win | 0 -> draw | -1 -> lose

    Given a board and the maximising player, it loops through the board, checks for an empty space, and if there is
    then it puts x into it and then calls itself, but switches the maximising player, so it then puts an o into the
    space, this keeps on going till the board is completed and it does an evaluation of the score, this is then
    compared to the worst score for that player (X's would be -infinity, and vice versa) as the variable best_score
    and if the new evaluation is better than best_score, it is assigned to best_score. it goes on to the next
    possible ending and then compares that to the current score and if it's better replaces it. this keeps on going
    till all endings are done and the best ending is saved, then you go one up and then check the next branch in the
    tree. It then checks all these branches, gets the best branch and goes up again, and repeats until it gets back
    to the top.

    :param board: the game board
    :param maximizing_player: the current player
    :return: what will happen if the ai makes a move in this slot
    """

    if evaluate_board(board) != "":
        return potential_scores[evaluate_board(board)]

    if maximizing_player:
        max_best_move = float('-inf')
        for i in range(9):
            if board[board_keys[i]] == '-':
                board[board_keys[i]] = "x"
                score = mini_max(board, False)
                board[board_keys[i]] = "-"
                if score > max_best_move:
                    max_best_move = score
        return max_best_move
    else:
        min_best_move = float('inf')
        for i in range(9):
            if board[board_keys[i]] == '-':
                board[board_keys[i]] = "o"
                score = mini_max(board, True)
                board[board_keys[i]] = "-"
                if score < min_best_move:
                    min_best_move = score
        return min_best_move


def get_ai_mode(config: dict, config_path: Path, ignore_default: bool = False, input_config: dict = None) -> list:
    """
    picks the ai mode or gives the option for the user to create their own ai mode
    :return: a list which decides the ai mode with three possible variables
    """
    default_name = config['default']
    ai_modes = config['ai_modes']
    mode_names = [ai_mode['Name'] for ai_mode in ai_modes]
    get_mode_index = dict((mode['Name'], index) for index, mode in enumerate(ai_modes))
    input_config = input_config if input_config is not None else default_input_config

    if default_name in mode_names and ignore_default is False:
        default_ai_mode = ai_modes[get_mode_index[default_name]]
        freq_lose = default_ai_mode['freq_lose']
        freq_draw = default_ai_mode['freq_draw']
        freq_win = default_ai_mode['freq_win']
    else:
        user_ai_mode_name = get_input(input_config['mode'])

        if user_ai_mode_name in mode_names:
            picked_mode = ai_modes[get_mode_index[user_ai_mode_name]]
            freq_lose = picked_mode['freq_lose']
            freq_draw = picked_mode['freq_draw']
            freq_win = picked_mode['freq_win']
            config['default'] = user_ai_mode_name
        else:
            user_ai_mode_name = get_input(input_config['mode_name'])
            freq_lose = int(get_input(input_config['freq_lose']))
            freq_draw = int(get_input(input_config['freq_draw']))
            freq_win = int(get_input(input_config['freq_win']))

            config['default'] = user_ai_mode_name
            config['ai_modes'].append({
                "Name": user_ai_mode_name,
                "freq_lose": freq_lose,
                "freq_draw": freq_draw,
                "freq_win": freq_win,
            })
    testing = all(input_config[key]['mode'] == 'test' for key in input_config.keys())
    if ignore_default is True or testing is True:
        with config_path.open(mode='w') as ai_mode_config_file:
            json.dump(config, ai_mode_config_file, indent=2)
    if ignore_default is False:
        return list((["lose"] * freq_lose) + (["draw"] * freq_draw) + (["win"] * freq_win))


def print_board(board: dict):
    """
    saves the three rows of the tic, tac toe board and into separate variables and then prints them with a space between
    each board
    :return: nothing
    """
    x_style = "\033[31m"
    o_style = "\033[34m"
    clear_style = "\033[0m"
    i = 0
    for key, value in board.items():
        i += 1
        if value == "-":
            board[key] = i
        if value == "x":
            board[key] = x_style + "X" + clear_style
        if value == "o":
            board[key] = o_style + "O" + clear_style

    return (f"""
    {board['top_left']}  {board['top_mid']}  {board['top_right']}
    {board['mid_left']}  {board['mid_mid']}  {board['mid_right']}
    {board['bot_left']}  {board['bot_mid']}  {board['bot_right']}
    """)


def tutorial(tutorial_path=tutorial_file_path) -> str:
    """
    Asks if they want to see the tutorial, and if they do prints a string explaining the game
    """
    with open(tutorial_path, 'r') as tutorial_file:
        return tutorial_file.read()


def tictactoe_game(input_config: dict = None, config: dict = None, config_path: Path = None) -> str:
    """
    checks if any one has one yet and if they have then return who is the winner, if no one has won then
    it picks the player who is going first, prints the board, asks the user to pick a slot and replaces the slot
    with the user's mark (x or o). After that, it checks if there is a tie (if so it breaks the loop) and if there
    isn't then it continues until the match has finished. Finally after the loop breaks it checks who won and then
    prints the winner.
    """
    input_config = input_config if input_config is not None else default_input_config
    config_path = config_path if config_path is not None else tic_config_path
    config = config if config is not None else tic_config
    picked_slot, config['picked_slots'], ai_mode_range, ai_player, end_state_of_game = None, [], [], "", ""
    players = ["player 1", "player 2"]
    get_player_symbol = {'player 1': 'x', 'player 2': 'o'}
    board = {key: "-" for key in board_keys}

    logger.info("the tic tac toe game has been picked")
    if config['tutorial_asked'] is False:
        user_wants_tutorial = get_input(input_config['tutorial'])
        if user_wants_tutorial:
            logger.info(tutorial())
        config['tutorial_asked'] = True
    ai_is_playing = get_input(input_config['human_or_ai'])
    current_player = get_input(input_config['player_going_first'], random.choice(players))
    if ai_is_playing is True:
        ai_player = get_input(input_config['ai_player'], random.choice(players))
        ai_mode_range = get_ai_mode(config, config_path)
    while end_state_of_game == '':
        logger.info(print_board(dict(board)))

        if ai_is_playing is False:
            logger.info(f"it is {current_player}'s turn")
            picked_slot = int(get_input(input_config['picked_slot']))
        elif ai_is_playing is True and ai_player != current_player:
            logger.info("it is your turn")
            picked_slot = int(get_input(input_config['picked_slot']))
        elif ai_is_playing is True and ai_player == current_player:
            logger.info("it is the AI's turn")
            if input_config['ai_pick']['mode'] == 'pass':  # pragma: no cover
                ai_mode = random.choice(ai_mode_range)
                picked_slot = int(get_input(input_config['ai_pick'], pick_ai_move(board, current_player, ai_mode))) + 1
            else:
                picked_slot = get_input(input_config['ai_pick'])

        config['picked_slots'].append(picked_slot)
        player_symbol = str(get_player_symbol[current_player])
        board[board_keys[picked_slot - 1]] = player_symbol
        end_state_of_game = evaluate_board(board, ai_player=ai_player)
        current_player = switch_player[current_player]

    with config_path.open(mode='w') as config_file:
        json.dump(config, config_file, indent=2)
    print(print_board(dict(board)))
    return evaluation_to_message[end_state_of_game]


def game() -> None:  # pragma: no cover
    """
    thing
    """
    logger.info(tictactoe_game())


if __name__ == '__main__':  # pragma: no cover
    game()
