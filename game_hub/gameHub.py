"""
An interface to play multiple games
"""
import random
import json
import shutil
import click
from prompt_toolkit.history import FileHistory
from pyfiglet import Figlet
from pathlib import Path
from difflib import get_close_matches
from typing import Callable


from game_hub.games import hangMan, ticTacToe, rockPaperScissors
from game_hub.games.gameCLI import change_last_game, gamehub_config_path
from game_hub.utils.logs.logger import system_dir
from game_hub.utils.logs import logger
from game_hub.utils.file.path import data_dir
from game_hub.clickRepl import repl


logger = logger.logging.getLogger('gamehub.gamehub')

last_game_played: Callable
last_game: Callable

default_tic_config_path = data_dir / 'default_ticTacToe.json'
default_gamehub_config_path = data_dir / 'default_gameHub.json'

str_to_game = {
    "tictactoe": ticTacToe.game,
    "hangman": hangMan.game,
    "rps": rockPaperScissors.game,
    None: print
}
game_to_str = {
    rockPaperScissors.rps_game: "rps",
    ticTacToe.game: "tictactoe",
    hangMan.game: "hangman"
}
cmd_to_str = {
    "tic": "tictactoe",
    "hang": "hangman",
    "rps": "rps",
}


def pick_ai_mode() -> None:
    """
    thing
    """
    ticTacToe.get_ai_mode(ticTacToe.tic_config,
                          ticTacToe.tic_config_path,
                          ignore_default=True)


def create_repl() -> None:
    """makes a repl"""
    f = Figlet(font='cosmic', width=94)
    logger.info(f.renderText('g a m e  h u b'))
    logger.info("To get the repl help menu, enter :h")
    logger.info("To get the gamehub help menu, enter -h or --help")
    command_history_file = system_dir / 'my_repl_history'
    command_history_file.parent.mkdir(parents=True, exist_ok=True)
    prompt_kwargs = {
        'history': FileHistory(command_history_file),
        'message': 'Gamehub> '}
    repl(click.get_current_context(), prompt_kwargs=prompt_kwargs)


def create_config(config_path_src: Path, config_path: Path, reset: bool = False) -> None:
    """
    copies a file to a new location
    """
    if config_path.exists() is False or reset is True:
        config_path.parent.mkdir(parents=True, exist_ok=True)
        shutil.copy(config_path_src, config_path)


def generate() -> Callable:
    """
    Pick Random Game to play
    :return: picked game
    """
    list_games = [rockPaperScissors.rps_game, hangMan.game, ticTacToe.game]
    picked_game = list_games[random.randint(0, 2)]
    change_last_game(game_to_str[picked_game], gamehub_config_path)
    return picked_game


def pick_rand_game(games: iter) -> Callable or None:
    """
    play's a random game from a given list. if one of the games is spelt wrong it prints the closest matching command
    in case they misspell them.
    :param games: list of games to randomly chose from
    """
    games = list(games)
    for game in games:
        if game not in cmd_to_str.keys():
            closest_matching_command = get_close_matches(game, cmd_to_str.keys())
            fail_message = f"{game} is not an available game"
            if closest_matching_command is not None:
                logger.info(fail_message + f"\nDid you mean: {closest_matching_command[0]}?")
            else:
                logger.info(fail_message + "\nSee all games by entering gamehub play or just play if you are in a repl")
            return None

    for i in range(len(games)):
        games[i] = cmd_to_str[games[i]]

    return random.choice(games)


def play_again(last_game_path: Path) -> None:
    """
    plays the last game you played
    """
    last_game_path.parent.mkdir(parents=True, exist_ok=True)
    last_game_config = json.loads(last_game_path.read_text())
    return last_game_config['last_game']
