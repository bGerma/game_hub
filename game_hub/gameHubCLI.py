"""
holds all commands
"""
import click

from game_hub.games import ticTacToe
from game_hub.utils.logs.logger import logging
from game_hub.games.gameCLI import play, gamehub_config_path
from game_hub import gameHub


gameHub.create_config(gameHub.default_tic_config_path, ticTacToe.tic_config_path, )
gameHub.create_config(gameHub.default_gamehub_config_path, gamehub_config_path, )

logger = logging.getLogger('gamehub.gameHubCLI')
CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)  # DONE
def cli() -> None:
    """
    gamehub is an interface for the three games we currently have in stock, which are tictactoe, hangman and rock paper
    scissors. You can play these games by typing 'gamehub play [game]' you can see the names of the games by typing in
    'gamehub play'. If you are in the repl, the commands are the same but you don't include the gamehub. As well as this
    , in the repl, you can press the tab key and a list of options appears that you can pick from.
    """
    pass


@cli.group('settings')  # DONE
def settings() -> None:
    """
    The gamehub settings
    """
    pass


@settings.group('tic')  # DONE
def tic_settings() -> None:
    """
    all the tictactoe_settings
    """
    pass


@tic_settings.command('pick_ai_mode')  # DONE
def pick_ai_mode_cmd() -> None:
    """
    adds an ai mode to the tictactoe config
    """
    gameHub.pick_ai_mode()


@tic_settings.command('tutorial')  # DONE
def tutorial_cmd() -> None:
    """
    prints the tutorial
    """
    logger.info(ticTacToe.tutorial())


@tic_settings.command('reset')
def reset_settings_cmd() -> None:
    """
    resets all settings to default
    """
    gameHub.create_config(gameHub.default_tic_config_path, ticTacToe.tic_config_path, reset=True)
    gameHub.create_config(gameHub.default_gamehub_config_path, gamehub_config_path, reset=True)


@cli.command('generate', short_help="pick random game to play")  # DONE
def generate_cmd() -> None:
    """
    Pick Random Game to play
    """
    gameHub.generate()()


@cli.command("last_game", short_help="plays the last game you played")  # DONE
def play_again_cmd() -> None:
    """
    plays the last game you played
    """
    gameHub.str_to_game[gameHub.play_again(gamehub_config_path)]()


@cli.command("play_list", short_help="play's a random game from a list")  # DONE
@click.option('--games', '-g', nargs=1, type=str,
              help="enter '-g/--games + game' and repeat with different game to make a list", multiple=True)
def play_game_from_list_cmd(games) -> None:
    """
    play's a random game from a list and prints the closest command if you misspell it.
    \f
    :param games: string will print
    """
    gameHub.str_to_game[gameHub.pick_rand_game(games)]()


@cli.command('repl', short_help="creates a repl and a exit command")  # DONE
def create_repl_cmd() -> None:
    """
    creates gamehub repl
    """
    @cli.command("exit", short_help="breaks you out of repl")
    def exit_repl_cmd() -> None:
        """
        exits repl
        """
        raise EOFError

    gameHub.create_repl()


cli.add_command(play)
