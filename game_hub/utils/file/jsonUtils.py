"""
contains utility functions for json files
"""
import json
from pathlib import Path

from game_hub.utils.file.config.schemaUtils import validator


def load_json_path(json_path: Path, validate_json: bool = True) -> dict:
    """
    loads json files from a given path
    :return: a json file converted to a python dictionary object that has been validated
    """
    json_path.parent.mkdir(parents=True, exist_ok=True)
    json_dict = json.loads(json_path.read_text())
    if validate_json is True:
        validator(json_dict)
    return json_dict
