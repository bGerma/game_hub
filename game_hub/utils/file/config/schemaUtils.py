"""
contains all utils related to schemas
"""
import json
import os
from difflib import get_close_matches

from jsonschema import validate
from pathlib import Path


def validator(config_json: dict, schema_dir: Path = None) -> None:
    """
    the better validator i hope maybe
    :return:
    """
    schema_dir = schema_dir if schema_dir is not None else Path(__file__).parent / 'schema'
    wanted_schema_version = config_json['schema_version']
    schema_versions = [file_name.split('_')[0] for file_name in os.listdir(schema_dir)]

    if wanted_schema_version in schema_versions:
        schema = json.loads(Path(schema_dir / f'{wanted_schema_version}_schema.json').read_text())
        validate(instance=config_json, schema=schema)
    else:
        closest_schema_version = get_close_matches(wanted_schema_version, schema_versions)
        error_message = f"{wanted_schema_version} not in: {schema_versions}"
        if closest_schema_version is not None:
            raise FileNotFoundError(error_message + f"\nDid you mean: {closest_schema_version}")
        else:
            raise FileNotFoundError(error_message)
