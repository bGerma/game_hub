"""
Utilities related to file paths
"""
import os
from pathlib import Path


gamehub_home_dir = Path.home() / '.game_hub'
system_dir = gamehub_home_dir / 'system'
configs = gamehub_home_dir / 'configs'


def project_root() -> Path:
    """
    gets the project root path
    :return: the project root path
    """
    path_of_this_file = Path(__file__)
    project_root_path = path_of_this_file.parent.parent.parent.parent
    return project_root_path


data_dir = project_root() / 'game_hub/data'


def find_file(file_name: str, start_path: str = './') -> str:
    """
    Find a file recursively from the star_path and return if found
    :param file_name:
    :param start_path:
    :return:
    """
    for root, dirs, files in os.walk(start_path):
        if file_name in files:
            return os.path.join(root, file_name)
