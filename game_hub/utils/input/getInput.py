"""
Gets the input
"""
from typing import Any
from PyInquirer import prompt
from examples import custom_style_1


example_prompt = {
    "mode": "prompt",
}


def get_input(input_config: dict, auto_value: Any = None) -> Any:
    """
    gets the input
    :param auto_value: the auto_value that is auto returned
    :param input_config: the
    :return: the input
    """
    _input = None

    if input_config['mode'] == 'prompt':
        description = input_config['prompt_desc']
        _input = prompt(description, style=custom_style_1)[description['name']]
    elif input_config['mode'] == 'manual':
        _input = input(input_config['cmd_line'])
    elif input_config['mode'] == 'test':
        generate_input = input_config.get('generator')
        if generate_input is not None:
            _input = generate_input.__next__()
        else:
            _input = input_config['input']
    elif input_config['mode'] == 'pass':
        return auto_value
    else:
        raise KeyError(f'{input_config["mode"]} is not in: prompt, manual, test, auto')

    return _input
