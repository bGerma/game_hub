"""
contains all the questions that need to be asked by the games
"""
import regex

from prompt_toolkit.validation import Validator, ValidationError


class StringValidator(Validator):  # pragma: no cover
    """
    thing
    """

    def validate(self, document) -> None:
        """
        validates the input
        """

        ok = regex.match('^[a-zA-Z]{1,11}$', document.text)
        if not ok:
            raise ValidationError(
                message='Please enter a string that is less than 10 characters',
                cursor_position=len(document.text)
            )
