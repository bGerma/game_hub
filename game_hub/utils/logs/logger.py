"""
Bonus:
- Make logs be pretty printed
"""
import os
import logging
import logging.config
import re
from datetime import datetime
from pathlib import Path
from typing import Union

from game_hub.utils.file.path import system_dir

logs_path = system_dir / 'logs'


def configure_logger(name: str, log_path: Union[str, Path]) -> logging.Logger:
    """
    configures the logger
    """
    if isinstance(log_path, str):
        log_path = Path(log_path)
    log_path.parent.mkdir(parents=True, exist_ok=True)

    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'message': {'format': '%(message)s'},
            'default': {'format': '%(asctime)s - %(name)-27s:[%(lineno)-4d] - function:%(funcName)-20s - '
                                  '%(levelname)-8s - %(message)s',
                        'datefmt': '%Y/%m/%d %I:%M:%S %p'}
        },
        'handlers': {
            'console': {
                "level": "INFO",
                "()": "game_hub.utils.logs.ClickHandler"
            },
            'log_to_file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'default',
                'filename': log_path,
                'backupCount': 0
            },
            'root': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': 'default',
                'filename': os.path.splitext(str(log_path))[0] + '_root.log',
                'backupCount': 0
            }
        },
        'loggers': {
            name: {
                'level': 'DEBUG',
                'handlers': ['console', 'log_to_file'],
                'filemode': 'w'
            },
            '': {
                'level': 'DEBUG',
                'handlers': ['root'],
                'filemode': 'w'
            }
        }
    })
    return logging.getLogger(name)


log_file = logs_path / f'gamehub_{datetime.today().date()}.log'
logger = configure_logger('gamehub', log_file)


def delete_old_logs() -> None:
    """
    deletes all old logs
    :return:
    """
    logs = os.listdir(logs_path)
    match_pattern = f"^gamehub_{datetime.today().date()}.*"
    for log in logs:
        is_old_log = not re.match(match_pattern, log)
        if is_old_log:
            os.remove(logs_path / log)


delete_old_logs()
