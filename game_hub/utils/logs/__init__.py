"""
this folder contains all the util functions
"""
import logging
import click


class ClickHandler(logging.Handler):
    """
    Click Handler for print to console well
    """
    _use_stderr = True
    colors = {
        'debug': dict(fg='blue'),
        'info': {},
        'ok': dict(fg='bright_green'),
        'warning': dict(fg='bright_yellow'),
        'exception': dict(fg='bright_red'),
        'error': dict(fg='bright_red'),
        'critical': dict(fg='bright_red')
    }

    def emit(self, record) -> None:
        """
        Do whatever it takes to actually log the specified logs record.

        This version is intended to be implemented by subclasses and so
        raises a NotImplementedError.
        """
        # noinspection PyBroadException
        try:
            msg = self.format(record)
            if record.levelno >= self.level:
                style_argument = record.__dict__.get('style', self.colors.get(record.levelname.lower(), {}))
                click.secho(msg, err=self._use_stderr, **style_argument)
        except Exception:
            self.handleError(record)
