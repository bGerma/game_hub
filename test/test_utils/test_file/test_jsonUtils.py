"""
tests the jsonUtils.py file
"""
import pytest

from game_hub.utils.file.jsonUtils import load_json_path
from test.test_game.test_ticTacToe import test_data_root


load_json_path_dir = test_data_root / 'dir_load_json_path'


@pytest.mark.parametrize('json_path, expected', [
    ("1", {"1": "test"}),
    ("2", {"2": "test"}),
    ("3", {"3": "test"}),
])
def test_load_json_path(json_path, expected) -> None:
    """
    tests the load_json_path() function
    """
    print(load_json_path_dir)
    actual_output = load_json_path(load_json_path_dir / f"{json_path}.json", validate_json=False)
    assert actual_output == expected
