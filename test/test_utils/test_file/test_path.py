"""
tests all functions in the path.py file
"""
import os
from pathlib import Path

from game_hub.utils.file import path


def test_find_file() -> None:
    """thing"""
    start_path = os.path.join(Path(__file__).parent, 'test_path_dirs')
    file_name = 'test_fileToFind.txt'
    actual_output = path.find_file(file_name, start_path)
    if os.name == "nt":
        expected_output = os.path.join(start_path, 'two\\test_fileToFind.txt')
    else:
        expected_output = os.path.join(start_path, 'two/test_fileToFind.txt')
    assert actual_output == expected_output


def test_project_root() -> None:
    """thing"""
    expected_output = Path(__file__).parent.parent.parent.parent
    actual_output = path.project_root()
    assert actual_output == expected_output
