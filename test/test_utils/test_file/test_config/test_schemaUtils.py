"""
contains all tests for schemaUtils.py
"""
import json
from pathlib import Path
from jsonschema import ValidationError

from game_hub.utils.file.config.schemaUtils import validator

parent_dir_path = Path(__file__).parent
test_config_dir_path = parent_dir_path / 'test_configs'
test_schema_dir_path = parent_dir_path / 'test_schemas'


def test_validator_not_found() -> None:
    """
    tests the validator: tests what happens when it can't find the schema
    """
    test_config_path = test_config_dir_path / 'notFound.json'
    test_config = json.loads(test_config_path.read_text())
    try:
        validator(test_config, test_schema_dir_path)
        assert False
    except FileNotFoundError:
        assert True


def test_validator_not_validated() -> None:
    """
    tests the validator: tests what happens when the validation fails
    """
    test_config_path = test_config_dir_path / 'notValidated.json'
    test_config = json.loads(test_config_path.read_text())
    try:
        validator(test_config, test_schema_dir_path)
        assert False
    except ValidationError:
        assert True


def test_validator_validated() -> None:
    """
    tests the validator: tests what happens when it validates
    """
    test_config_path = test_config_dir_path / 'validated.json'
    test_config = json.loads(test_config_path.read_text())
    try:
        validator(test_config, test_schema_dir_path)
        assert True
    except FileNotFoundError or ValidationError:
        assert False
