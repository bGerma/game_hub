"""
tests the getInput.py file
"""
import pytest
from game_hub.utils.input import getInput
try:
    from prompt_toolkit.terminal import win32_output
except ImportError:
    win32_output = None


@pytest.mark.parametrize('test_input', [1, 2, 3, 4, 5])
def test_get_input_test_input_mode(test_input) -> None:
    """thing"""
    test_input_config = {"mode": "test", "input": test_input}
    expected_output = test_input
    actual_output = getInput.get_input(test_input_config)
    assert expected_output == actual_output


@pytest.mark.parametrize('test_inputs', [
    [11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20],
    [21, 22, 23, 24, 25],
    [26, 27, 28, 29, 30],
    [31, 32, 33, 34, 35],
    [36, 37, 38, 39, 40],
])
def test_get_input_test_generator_mode(test_inputs) -> None:
    """thing"""
    test_input_config = {"mode": "test", "generator": (num for num in test_inputs)}
    for num in test_inputs:
        num_generator = getInput.get_input(test_input_config)
        assert num == num_generator


def test_get_input_input_mode() -> None:
    """thing"""
    test_input_config = {"mode": "manual", "cmd_line": "|>"}
    try:
        _ = getInput.get_input(test_input_config)
        assert False
    except OSError:
        assert True


def test_get_input_user_input_mode() -> None:
    """thing"""
    if win32_output is not None:
        test_input_config = {
            'mode': 'prompt',
            'prompt_desc': {'type': 'list', 'name': '1', 'message': '1: ', 'choices': ['1', '2']}
        }
        try:
            _ = getInput.get_input(test_input_config)
            assert False
        except win32_output.NoConsoleScreenBufferError:
            assert True
    else:
        assert True
