"""
tests the tic tac toe module
"""
import json

import pytest

from game_hub.games import ticTacToe
from game_hub.utils.file.jsonUtils import load_json_path
from game_hub.utils.file.path import project_root

test_data_root = project_root() / 'test/test_data'
test_data_config_dir = test_data_root / 'dir_get_ai_mode'

_1, _2, _3, _4, _5, _6, _7, _8, _9 = [i + 1 for i in range(9)]
_X, _O = "\x1b[31mX\x1b[0m", "\x1b[34mO\x1b[0m"


@pytest.mark.parametrize("board, ai_player, expected", [
    # checks middle column for player 1
    ({"top_left": "-", "top_mid": "x", "top_right": "-",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "-", "bot_mid": "x", "bot_right": "-"}, "", "player 1"),
    # checks right column for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "x",
      "mid_left": "-", "mid_mid": "-", "mid_right": "x",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "", "player 1"),
    # checks left column for player 1
    ({"top_left": "x", "top_mid": "-", "top_right": "-",
      "mid_left": "x", "mid_mid": "-", "mid_right": "-",
      "bot_left": "x", "bot_mid": "-", "bot_right": "-"}, "", "player 1"),
    # checks top row for player 1
    ({"top_left": "x", "top_mid": "x", "top_right": "x",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "", "player 1"),
    # checks middle row for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "x", "mid_mid": "x", "mid_right": "x",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "", "player 1"),
    # checks bottom row for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "x", "bot_mid": "x", "bot_right": "x"}, "", "player 1"),
    # checks bot-left to top-right diagonal for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "x",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "-", "bot_right": "-"}, "", "player 1"),
    # checks top-left to bot-right diagonal for player 1
    ({"top_left": "x", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "", "player 1"),
    # checks middle column for player 2
    ({"top_left": "-", "top_mid": "o", "top_right": "-",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "-", "bot_mid": "o", "bot_right": "-"}, "", "player 2"),
    # checks right column for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "o",
      "mid_left": "-", "mid_mid": "-", "mid_right": "o",
      "bot_left": "-", "bot_mid": "-", "bot_right": "o"}, "", "player 2"),
    # checks left column for player 2
    ({"top_left": "o", "top_mid": "-", "top_right": "-",
      "mid_left": "o", "mid_mid": "-", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "-"}, "", "player 2"),
    # checks top row for player 2
    ({"top_left": "o", "top_mid": "o", "top_right": "o",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "", "player 2"),
    # checks middle row for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "o", "mid_mid": "o", "mid_right": "o",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "", "player 2"),
    # checks bottom row for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "o", "bot_mid": "o", "bot_right": "o"}, "", "player 2"),
    # checks bot-left to top-right diagonal for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "o",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "-"}, "", "player 2"),
    # checks top-left to bot-right diagonal for player 2
    ({"top_left": "o", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "o"}, "", "player 2"),
    # checks for tie
    ({"top_left": "o", "top_mid": "x", "top_right": "o",
      "mid_left": "o", "mid_mid": "x", "mid_right": "o",
      "bot_left": "x", "bot_mid": "o", "bot_right": "x"}, "", "tie"),
    # checks for if the game is still going on
    ({"top_left": "o", "top_mid": "x", "top_right": "-",
      "mid_left": "o", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "o", "bot_right": "-"}, "", ""),
    # Part 2 - Electric Boogaloo --------------------------------------------------------------
    ({"top_left": "-", "top_mid": "x", "top_right": "-",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "-", "bot_mid": "x", "bot_right": "-"}, "player 1", "ai"),
    # checks right column for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "x",
      "mid_left": "-", "mid_mid": "-", "mid_right": "x",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "player 1", "ai"),
    # checks left column for player 1
    ({"top_left": "x", "top_mid": "-", "top_right": "-",
      "mid_left": "x", "mid_mid": "-", "mid_right": "-",
      "bot_left": "x", "bot_mid": "-", "bot_right": "-"}, "player 1", "ai"),
    # checks top row for player 1
    ({"top_left": "x", "top_mid": "x", "top_right": "x",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 1", "ai"),
    # checks middle row for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "x", "mid_mid": "x", "mid_right": "x",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 1", "ai"),
    # checks bottom row for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "x", "bot_mid": "x", "bot_right": "x"}, "player 1", "ai"),
    # checks bot-left to top-right diagonal for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "x",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "-", "bot_right": "-"}, "player 1", "ai"),
    # checks top-left to bot-right diagonal for player 1
    ({"top_left": "x", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "player 1", "ai"),
    # checks middle column for player 2
    ({"top_left": "-", "top_mid": "o", "top_right": "-",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "-", "bot_mid": "o", "bot_right": "-"}, "player 1", "human"),
    # checks right column for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "o",
      "mid_left": "-", "mid_mid": "-", "mid_right": "o",
      "bot_left": "-", "bot_mid": "-", "bot_right": "o"}, "player 1", "human"),
    # checks left column for player 2
    ({"top_left": "o", "top_mid": "-", "top_right": "-",
      "mid_left": "o", "mid_mid": "-", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "-"}, "player 1", "human"),
    # checks top row for player 2
    ({"top_left": "o", "top_mid": "o", "top_right": "o",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 1", "human"),
    # checks middle row for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "o", "mid_mid": "o", "mid_right": "o",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 1", "human"),
    # checks bottom row for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "o", "bot_mid": "o", "bot_right": "o"}, "player 1", "human"),
    # checks bot-left to top-right diagonal for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "o",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "-"}, "player 1", "human"),
    # checks top-left to bot-right diagonal for player 2
    ({"top_left": "o", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "o"}, "player 1", "human"),
    # checks for tie
    ({"top_left": "o", "top_mid": "x", "top_right": "o",
      "mid_left": "o", "mid_mid": "x", "mid_right": "o",
      "bot_left": "x", "bot_mid": "o", "bot_right": "x"}, "player 1", "tie"),
    # checks for if the game is still going on
    ({"top_left": "o", "top_mid": "x", "top_right": "-",
      "mid_left": "o", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "o", "bot_right": "-"}, "player 1", ""),
    # Part 3 - 3
    ({"top_left": "-", "top_mid": "x", "top_right": "-",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "-", "bot_mid": "x", "bot_right": "-"}, "player 2", "human"),
    # checks right column for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "x",
      "mid_left": "-", "mid_mid": "-", "mid_right": "x",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "player 2", "human"),
    # checks left column for player 1
    ({"top_left": "x", "top_mid": "-", "top_right": "-",
      "mid_left": "x", "mid_mid": "-", "mid_right": "-",
      "bot_left": "x", "bot_mid": "-", "bot_right": "-"}, "player 2", "human"),
    # checks top row for player 1
    ({"top_left": "x", "top_mid": "x", "top_right": "x",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "human"),
    # checks middle row for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "x", "mid_mid": "x", "mid_right": "x",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "human"),
    # checks bottom row for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "x", "bot_mid": "x", "bot_right": "x"}, "player 2", "human"),
    # checks bot-left to top-right diagonal for player 1
    ({"top_left": "-", "top_mid": "-", "top_right": "x",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "-", "bot_right": "-"}, "player 2", "human"),
    # checks top-left to bot-right diagonal for player 1
    ({"top_left": "x", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "player 2", "human"),
    # checks middle column for player 2
    ({"top_left": "-", "top_mid": "o", "top_right": "-",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "-", "bot_mid": "o", "bot_right": "-"}, "player 2", "ai"),
    # checks right column for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "o",
      "mid_left": "-", "mid_mid": "-", "mid_right": "o",
      "bot_left": "-", "bot_mid": "-", "bot_right": "o"}, "player 2", "ai"),
    # checks left column for player 2
    ({"top_left": "o", "top_mid": "-", "top_right": "-",
      "mid_left": "o", "mid_mid": "-", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "-"}, "player 2", "ai"),
    # checks top row for player 2
    ({"top_left": "o", "top_mid": "o", "top_right": "o",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "ai"),
    # checks middle row for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "o", "mid_mid": "o", "mid_right": "o",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "ai"),
    # checks bottom row for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "o", "bot_mid": "o", "bot_right": "o"}, "player 2", "ai"),
    # checks bot-left to top-right diagonal for player 2
    ({"top_left": "-", "top_mid": "-", "top_right": "o",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "-"}, "player 2", "ai"),
    # checks top-left to bot-right diagonal for player 2
    ({"top_left": "o", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "o"}, "player 2", "ai"),
    # checks for tie
    ({"top_left": "o", "top_mid": "x", "top_right": "o",
      "mid_left": "o", "mid_mid": "x", "mid_right": "o",
      "bot_left": "x", "bot_mid": "o", "bot_right": "x"}, "player 2", "tie"),
    # checks for if the game is still going on
    ({"top_left": "o", "top_mid": "x", "top_right": "-",
      "mid_left": "o", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "o", "bot_right": "-"}, "player 2", ""),
])
def test_evaluate_board(board: dict, ai_player, expected: str) -> None:
    """
    Testing all combinations of the board
    """
    actual_output = ticTacToe.evaluate_board(board, ai_player)
    assert actual_output == expected


@pytest.mark.parametrize("board, maximising_player, expected", [
    ({"top_left": "-", "top_mid": "x", "top_right": "x",
      "mid_left": "x", "mid_mid": "-", "mid_right": "o",
      "bot_left": "x", "bot_mid": "o", "bot_right": "o"}, False, 1),

    ({"top_left": "x", "top_mid": "-", "top_right": "o",
      "mid_left": "o", "mid_mid": "-", "mid_right": "x",
      "bot_left": "x", "bot_mid": "x", "bot_right": "-"}, False, 0),

    ({"top_left": "o", "top_mid": "-", "top_right": "x",
      "mid_left": "x", "mid_mid": "-", "mid_right": "o",
      "bot_left": "o", "bot_mid": "o", "bot_right": "-"}, False, -1)
])
def test_mini_max(board: dict, maximising_player: bool, expected: int) -> None:
    """
    Testing all combinations of the board
    """

    actual_output = ticTacToe.mini_max(board, maximising_player)
    print(actual_output)
    print(expected)
    assert actual_output == expected


@pytest.mark.parametrize("board, player_going_first, ai_move, expected", [
    # 1 / board0 - Player 1 - attacking -------------------------------------------------------
    ({"top_left": "-", "top_mid": "-", "top_right": "/",
      "mid_left": "x", "mid_mid": "/", "mid_right": "/",
      "bot_left": "x", "bot_mid": "/", "bot_right": "/"}, "player 1", "win", 0),
    # 2 / board1
    ({"top_left": "/", "top_mid": "-", "top_right": "-",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "x", "bot_right": "/"}, "player 1", "win", 1),
    # 3 / board2
    ({"top_left": "-", "top_mid": "/", "top_right": "-",
      "mid_left": "/", "mid_mid": "/", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "x"}, "player 1", "win", 2),
    # 4 / board3
    ({"top_left": "-", "top_mid": "x", "top_right": "x",
      "mid_left": "/", "mid_mid": "/", "mid_right": "/",
      "bot_left": "/", "bot_mid": "-", "bot_right": "/"}, "player 1", "win", 0),
    # 5 / board4
    ({"top_left": "-", "top_mid": "/", "top_right": "/",
      "mid_left": "-", "mid_mid": "x", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 1", "win", 3),
    # 6 / board5
    ({"top_left": "-", "top_mid": "/", "top_right": "/",
      "mid_left": "/", "mid_mid": "/", "mid_right": "/",
      "bot_left": "-", "bot_mid": "x", "bot_right": "x"}, "player 1", "win", 6),
    # 7 / board6
    ({"top_left": "-", "top_mid": "/", "top_right": "/",
      "mid_left": "-", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "/", "bot_right": "x"}, "player 1", "win", 0),
    # 8 / board7
    ({"top_left": "/", "top_mid": "/", "top_right": "-",
      "mid_left": "/", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "/", "bot_right": "/"}, "player 1", "win", 2),
    # 9 / board8
    ({"top_left": "/", "top_mid": "x", "top_right": "/",
      "mid_left": "-", "mid_mid": "-", "mid_right": "/",
      "bot_left": "/", "bot_mid": "x", "bot_right": "/"}, "player 1", "win", 4),

    # 1 / board9 - Player 2 - Blocking --------------------------------------------------------

    ({"top_left": "x", "top_mid": "/", "top_right": "/",
      "mid_left": "x", "mid_mid": "/", "mid_right": "/",
      "bot_left": "-", "bot_mid": "/", "bot_right": "-"}, "player 2", "win", 6),
    # 2 / board10
    ({"top_left": "/", "top_mid": "x", "top_right": "/",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "-", "bot_right": "-"}, "player 2", "win", 7),
    # 3 / board12
    ({"top_left": "/", "top_mid": "/", "top_right": "x",
      "mid_left": "/", "mid_mid": "/", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "-"}, "player 2", "win", 8),
    # 4 / board13
    ({"top_left": "x", "top_mid": "x", "top_right": "-",
      "mid_left": "/", "mid_mid": "/", "mid_right": "-",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 2", "win", 2),
    # 5 / board14
    ({"top_left": "/", "top_mid": "/", "top_right": "-",
      "mid_left": "x", "mid_mid": "x", "mid_right": "-",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 2", "win", 5),
    # 6 / board15
    ({"top_left": "/", "top_mid": "/", "top_right": "/",
      "mid_left": "/", "mid_mid": "/", "mid_right": "-",
      "bot_left": "x", "bot_mid": "x", "bot_right": "-"}, "player 2", "win", 8),
    # 7 / board16
    ({"top_left": "x", "top_mid": "/", "top_right": "/",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "-", "bot_right": "-"}, "player 2", "win", 8),
    # 8 / board17
    ({"top_left": "/", "top_mid": "-", "top_right": "-",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "x", "bot_mid": "/", "bot_right": "/"}, "player 2", "win", 2),
    # 9 / board18
    ({"top_left": "/", "top_mid": "-", "top_right": "/",
      "mid_left": "x", "mid_mid": "-", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 1", "win", 4),

    # 1 / board19 - DRAW - Player 1 -----------------------------------------------------------

    ({"top_left": "-", "top_mid": "-", "top_right": "/",
      "mid_left": "x", "mid_mid": "/", "mid_right": "/",
      "bot_left": "x", "bot_mid": "/", "bot_right": "/"}, "player 1", "draw", 1),
    # 2 / board20
    ({"top_left": "/", "top_mid": "-", "top_right": "-",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "x", "bot_right": "/"}, "player 1", "draw", 2),
    # 3 / board21
    ({"top_left": "-", "top_mid": "/", "top_right": "-",
      "mid_left": "/", "mid_mid": "/", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "x"}, "player 1", "draw", 0),
    # 4 / board22
    ({"top_left": "-", "top_mid": "x", "top_right": "x",
      "mid_left": "/", "mid_mid": "/", "mid_right": "/",
      "bot_left": "/", "bot_mid": "-", "bot_right": "/"}, "player 1", "draw", 7),
    # 5 / board23
    ({"top_left": "-", "top_mid": "/", "top_right": "/",
      "mid_left": "-", "mid_mid": "x", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 1", "draw", 0),
    # 6 / board24
    ({"top_left": "-", "top_mid": "/", "top_right": "/",
      "mid_left": "/", "mid_mid": "/", "mid_right": "/",
      "bot_left": "-", "bot_mid": "x", "bot_right": "x"}, "player 1", "draw", 0),
    # 7 / board25
    ({"top_left": "-", "top_mid": "/", "top_right": "/",
      "mid_left": "-", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "/", "bot_right": "x"}, "player 1", "draw", 3),
    # 8 / board26
    ({"top_left": "/", "top_mid": "/", "top_right": "-",
      "mid_left": "/", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "/", "bot_right": "/"}, "player 1", "draw", 5),
    # 9 / board27
    ({"top_left": "o", "top_mid": "x", "top_right": "/",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "o", "bot_mid": "x", "bot_right": "/"}, "player 1", "draw", 3),
    # temporary
    ({"top_left": "o", "top_mid": "o", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "player 1", "draw", 2),

    # 1 / board28 - Player 2 - Blocking -------------------------------------------------------

    ({"top_left": "x", "top_mid": "/", "top_right": "/",
      "mid_left": "x", "mid_mid": "/", "mid_right": "/",
      "bot_left": "-", "bot_mid": "/", "bot_right": "-"}, "player 2", "draw", 6),
    # 2 / board29
    ({"top_left": "/", "top_mid": "x", "top_right": "/",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "-", "bot_right": "-"}, "player 2", "draw", 7),
    # 3 / board30
    ({"top_left": "/", "top_mid": "/", "top_right": "x",
      "mid_left": "/", "mid_mid": "/", "mid_right": "x",
      "bot_left": "-", "bot_mid": "/", "bot_right": "-"}, "player 2", "draw", 8),
    # 4 / board31
    ({"top_left": "x", "top_mid": "x", "top_right": "-",
      "mid_left": "/", "mid_mid": "/", "mid_right": "-",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 2", "draw", 2),
    # 5 / board32
    ({"top_left": "/", "top_mid": "/", "top_right": "-",
      "mid_left": "x", "mid_mid": "x", "mid_right": "-",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 2", "draw", 5),
    # 6 / board33
    ({"top_left": "/", "top_mid": "/", "top_right": "/",
      "mid_left": "/", "mid_mid": "/", "mid_right": "-",
      "bot_left": "x", "bot_mid": "x", "bot_right": "-"}, "player 2", "draw", 8),
    # 7 / board34
    ({"top_left": "x", "top_mid": "/", "top_right": "/",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "/", "bot_mid": "-", "bot_right": "-"}, "player 2", "draw", 8),
    # 8 / board35
    ({"top_left": "/", "top_mid": "-", "top_right": "-",
      "mid_left": "/", "mid_mid": "x", "mid_right": "/",
      "bot_left": "x", "bot_mid": "/", "bot_right": "/"}, "player 2", "draw", 2),
    # 9 / board36
    ({"top_left": "/", "top_mid": "-", "top_right": "/",
      "mid_left": "x", "mid_mid": "-", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "/"}, "player 2", "draw", 4),
    # temporary
    ({"top_left": "o", "top_mid": "o", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "x"}, "player 2", "draw", 2),

    # 1 / board37 - LOSE - player 1 - Attacking -----------------------------------------------

    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "o", "mid_mid": "x", "mid_right": "/",
      "bot_left": "o", "bot_mid": "x", "bot_right": "/"}, "player 1", "lose", 2),
    # 2 / board38
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "/", "mid_mid": "o", "mid_right": "x",
      "bot_left": "/", "bot_mid": "o", "bot_right": "x"}, "player 1", "lose", 0),
    # 3 / board39
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "/", "mid_mid": "x", "mid_right": "o",
      "bot_left": "/", "bot_mid": "x", "bot_right": "o"}, "player 1", "lose", 0),
    # 4 / board41
    ({"top_left": "-", "top_mid": "o", "top_right": "o",
      "mid_left": "-", "mid_mid": "x", "mid_right": "x",
      "bot_left": "-", "bot_mid": "/", "bot_right": "/"}, "player 1", "lose", 6),
    # 5 / board42
    ({"top_left": "-", "top_mid": "x", "top_right": "x",
      "mid_left": "-", "mid_mid": "o", "mid_right": "o",
      "bot_left": "-", "bot_mid": "/", "bot_right": "/"}, "player 1", "lose", 6),
    # 6 / board43
    ({"top_left": "-", "top_mid": "/", "top_right": "/",
      "mid_left": "-", "mid_mid": "x", "mid_right": "x",
      "bot_left": "-", "bot_mid": "o", "bot_right": "o"}, "player 1", "lose", 0),
    # 7 / board44
    ({"top_left": "-", "top_mid": "x", "top_right": "x",
      "mid_left": "/", "mid_mid": "o", "mid_right": "-",
      "bot_left": "/", "bot_mid": "/", "bot_right": "o"}, "player 1", "lose", 5),
    # 8 / board45
    ({"top_left": "x", "top_mid": "x", "top_right": "-",
      "mid_left": "-", "mid_mid": "o", "mid_right": "/",
      "bot_left": "o", "bot_mid": "/", "bot_right": "/"}, "player 1", "lose", 3),
    # 9 / board46
    ({"top_left": "/", "top_mid": "-", "top_right": "/",
      "mid_left": "o", "mid_mid": "-", "mid_right": "o",
      "bot_left": "x", "bot_mid": "-", "bot_right": "x"}, "player 1", "lose", 1),

    # 1 / board47 - Player 2 - Blocking -------------------------------------------------------

    ({"top_left": "o", "top_mid": "x", "top_right": "/",
      "mid_left": "o", "mid_mid": "x", "mid_right": "/",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "lose", 8),
    # 2 / board48
    ({"top_left": "x", "top_mid": "o", "top_right": "/",
      "mid_left": "x", "mid_mid": "o", "mid_right": "/",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "lose", 8),
    # 3 / board49
    ({"top_left": "x", "top_mid": "/", "top_right": "o",
      "mid_left": "x", "mid_mid": "/", "mid_right": "o",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "lose", 7),
    # 4 / board50
    ({"top_left": "o", "top_mid": "o", "top_right": "-",
      "mid_left": "/", "mid_mid": "/", "mid_right": "-",
      "bot_left": "x", "bot_mid": "x", "bot_right": "-"}, "player 2", "lose", 5),
    # 5 / board51
    ({"top_left": "/", "top_mid": "/", "top_right": "-",
      "mid_left": "o", "mid_mid": "o", "mid_right": "-",
      "bot_left": "x", "bot_mid": "x", "bot_right": "-"}, "player 2", "lose", 2),
    # 6 / board52
    ({"top_left": "x", "top_mid": "x", "top_right": "-",
      "mid_left": "/", "mid_mid": "/", "mid_right": "-",
      "bot_left": "o", "bot_mid": "o", "bot_right": "-"}, "player 2", "lose", 5),
    # 7 / board53
    ({"top_left": "o", "top_mid": "-", "top_right": "x",
      "mid_left": "/", "mid_mid": "o", "mid_right": "x",
      "bot_left": "/", "bot_mid": "/", "bot_right": "-"}, "player 2", "lose", 1),
    # 8 / board54
    ({"top_left": "x", "top_mid": "-", "top_right": "o",
      "mid_left": "x", "mid_mid": "o", "mid_right": "/",
      "bot_left": "-", "bot_mid": "/", "bot_right": "/"}, "player 2", "lose", 1),
    # 9 / board55
    ({"top_left": "x", "top_mid": "o", "top_right": "/",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "x", "bot_mid": "o", "bot_right": "/"}, "player 2", "lose", 5),
    # Empty boards - 1 / board56 --------------------------------------------------------------------
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 1", "lose", [0, 1, 2, 3, 4, 5, 6, 7, 8]),
    # 2 / board57
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 1", "draw", [0, 1, 2, 3, 4, 5, 6, 7, 8]),
    # 2 / board58
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 1", "win", [0, 1, 2, 3, 4, 5, 6, 7, 8]),
    # 2 / board59
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "lose", [0, 1, 2, 3, 4, 5, 6, 7, 8]),
    # 2 / board60
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "draw", [0, 1, 2, 3, 4, 5, 6, 7, 8]),
    # 2 / board61
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, "player 2", "win", [0, 1, 2, 3, 4, 5, 6, 7, 8]),

])
def test_pick_ai_move(board: dict, player_going_first: str, ai_move: str, expected: int or list) -> None:
    """
    testing function best move for ai
    """
    actual_output = ticTacToe.pick_ai_move(board, player_going_first, ai_move)
    if isinstance(expected, list):
        assert actual_output in expected
    else:
        assert actual_output == expected


@pytest.mark.parametrize('ai_mode_name, expected_output, expected_saved', [
    ('1', list((['lose'] * 1) + (['draw'] * 2) + (['win'] * 3)), {
        'Name': '1',
        "freq_lose": 1,
        "freq_draw": 2,
        "freq_win": 3
    }),
    ('2', list((['lose'] * 12) + (['draw'] * 8) + (['win'] * 19)), {
        'Name': '2',
        "freq_lose": 12,
        "freq_draw": 8,
        "freq_win": 19
    }),
    ('3', list((['lose'] * 68) + (['draw'] * 419) + (['win'] * 68)), {
        'Name': '3',
        "freq_lose": 68,
        "freq_draw": 419,
        "freq_win": 68
    }),
    ('4', list((['lose'] * 74) + (['draw'] * 31) + (['win'] * 87)), {
        'Name': '4',
        "freq_lose": 74,
        "freq_draw": 31,
        "freq_win": 87
    }),
])
def test_get_ai_mode_default(ai_mode_name, expected_saved, expected_output) -> None:
    """
    tests that get_ai_mode will choose the default if there is any
    :return:
    """
    test_config_path = test_data_config_dir / 'default_mode.json'
    test_config = load_json_path(test_config_path, validate_json=False)
    ai_modes = test_config['ai_modes']
    get_mode_index = dict((mode['Name'], index) for index, mode in enumerate(ai_modes))

    test_config['default'] = ai_mode_name
    actual_output = ticTacToe.get_ai_mode(test_config, test_config_path)
    assert test_config['ai_modes'][get_mode_index[ai_mode_name]] == expected_saved
    assert actual_output == expected_output
    test_config['default'] = '_default_'


@pytest.mark.parametrize('debug_choices, expected_output, expected_saved', [
    (['1', 12, 34, 56], list((['lose'] * 12) + (['draw'] * 34) + (['win'] * 56)), {
        'Name': '1',
        "freq_lose": 12,
        "freq_draw": 34,
        "freq_win": 56
    }),
    (['2', 42, 54, 23], list((['lose'] * 42) + (['draw'] * 54) + (['win'] * 23)), {
        'Name': '2',
        "freq_lose": 42,
        "freq_draw": 54,
        "freq_win": 23
    }),
    (['3', 68, 41, 71], list((['lose'] * 68) + (['draw'] * 41) + (['win'] * 71)), {
        'Name': '3',
        "freq_lose": 68,
        "freq_draw": 41,
        "freq_win": 71
    }),
    (['4', 94, 31, 87], list((['lose'] * 94) + (['draw'] * 31) + (['win'] * 87)), {
        'Name': '4',
        "freq_lose": 94,
        "freq_draw": 31,
        "freq_win": 87
    }),
])
def test_get_ai_mode_natural_pick_mode(debug_choices, expected_output, expected_saved, ) -> None:
    """
    tests that get_ai_mode will choose the default if there is any
    :return:
    """
    test_config_path = test_data_config_dir / 'natural_pick_mode.json'
    default_config = load_json_path(test_config_path, validate_json=False)
    test_input_config = {
        'mode': {'mode': 'test', 'input': None},
        'mode_name': {'mode': 'test', 'input': debug_choices[0]},
        'freq_lose': {'mode': 'test', 'input': debug_choices[1]},
        'freq_draw': {'mode': 'test', 'input': debug_choices[2]},
        'freq_win': {'mode': 'test', 'input': debug_choices[3]},
    }
    actual_output = ticTacToe.get_ai_mode(
        load_json_path(test_config_path, validate_json=False), test_config_path,
        input_config=test_input_config
    )
    changed_config = load_json_path(test_config_path, validate_json=False)
    print(json.dumps(changed_config, indent=4))
    with open(test_config_path, 'w') as test_config_file:
        json.dump(default_config, test_config_file, indent=2)
    assert changed_config['ai_modes'][0] == expected_saved
    assert actual_output == expected_output


@pytest.mark.parametrize('debug_choices, expected', [
    (['1', 12, 34, 56], {
        'Name': '1',
        "freq_lose": 12,
        "freq_draw": 34,
        "freq_win": 56
    }),
    (['2', 42, 54, 23], {
        'Name': '2',
        "freq_lose": 42,
        "freq_draw": 54,
        "freq_win": 23
    }),
    (['3', 68, 41, 71], {
        'Name': '3',
        "freq_lose": 68,
        "freq_draw": 41,
        "freq_win": 71
    }),
    (['4', 94, 31, 87], {
        'Name': '4',
        "freq_lose": 94,
        "freq_draw": 31,
        "freq_win": 87
    }),
])
def test_get_ai_mode_unnatural_pick_mode(debug_choices, expected) -> None:
    """
    tests that get_ai_mode will choose the default if there is any
    :return:
    """
    test_input_config = {
        'mode': {'mode': 'test', 'input': None},
        'mode_name': {'mode': 'test', 'input': debug_choices[0]},
        'freq_lose': {'mode': 'test', 'input': debug_choices[1]},
        'freq_draw': {'mode': 'test', 'input': debug_choices[2]},
        'freq_win': {'mode': 'test', 'input': debug_choices[3]},
    }
    test_config_path = test_data_config_dir / 'unnatural_pick_mode.json'
    default_config = load_json_path(test_config_path, validate_json=False)
    ticTacToe.get_ai_mode(
        config=load_json_path(test_config_path, validate_json=False),
        config_path=test_config_path,
        ignore_default=True,
        input_config=test_input_config
    )
    changed_config = load_json_path(test_config_path, validate_json=False)
    print(json.dumps(changed_config['ai_modes'][1], indent=4))
    with open(test_config_path, 'w') as test_config_file:
        json.dump(default_config, test_config_file, indent=2)
    assert changed_config['ai_modes'][1] == expected


@pytest.mark.parametrize('mode_name, expected_output', [
    ('1', list((['lose'] * 1) + (['draw'] * 2) + (['win'] * 3))),
    ('2', list((['lose'] * 2) + (['draw'] * 4) + (['win'] * 6))),
    ('3', list((['lose'] * 3) + (['draw'] * 6) + (['win'] * 9))),
    ('4', list((['lose'] * 4) + (['draw'] * 8) + (['win'] * 12)))
])
def test_get_ai_mode_pick_made_mode(mode_name, expected_output) -> None:
    """b"""
    test_config_path = test_data_config_dir / 'pick_made_mode.json'
    test_config = load_json_path(test_config_path, validate_json=False)
    test_config['default'] = None
    test_input_config = {
        'mode': {'mode': 'test', 'input': mode_name}
    }
    actual_output = ticTacToe.get_ai_mode(
        dict(test_config),
        test_config_path,
        input_config=test_input_config
    )
    assert expected_output == actual_output


@pytest.mark.parametrize('tutorial_path, expected', [
    (test_data_root / 'test_tutorial.txt', "hello\nhello\nhello\nhello"),
    (None, "You")
])
def test_tutorial(tutorial_path, expected) -> None:
    """
    This tests the
    :param tutorial_path:
    :param expected:
    :return:
    """
    if tutorial_path:
        actual_output = ticTacToe.tutorial(tutorial_path)
    else:
        actual_output = ticTacToe.tutorial().split()[0]
    assert actual_output == expected


@pytest.mark.parametrize('board, expected', [
    ({"top_left": "-", "top_mid": "-", "top_right": "-",
      "mid_left": "-", "mid_mid": "-", "mid_right": "-",
      "bot_left": "-", "bot_mid": "-", "bot_right": "-"}, f"""
    {_1}  {_2}  {_3}
    {_4}  {_5}  {_6}
    {_7}  {_8}  {_9}
    """),
    ({"top_left": "x", "top_mid": "o", "top_right": "-",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "-"}, f"""
    {_X}  {_O}  {_3}
    {_4}  {_X}  {_6}
    {_O}  {_8}  {_9}
    """),
    ({"top_left": "x", "top_mid": "x", "top_right": "x",
      "mid_left": "-", "mid_mid": "o", "mid_right": "-",
      "bot_left": "o", "bot_mid": "-", "bot_right": "o"}, f"""
    {_X}  {_X}  {_X}
    {_4}  {_O}  {_6}
    {_O}  {_8}  {_O}
    """),
    ({"top_left": "o", "top_mid": "o", "top_right": "x",
      "mid_left": "o", "mid_mid": "-", "mid_right": "o",
      "bot_left": "-", "bot_mid": "o", "bot_right": "o"}, f"""
    {_O}  {_O}  {_X}
    {_O}  {_5}  {_O}
    {_7}  {_O}  {_O}
    """),
    ({"top_left": "-", "top_mid": "o", "top_right": "x",
      "mid_left": "-", "mid_mid": "x", "mid_right": "-",
      "bot_left": "x", "bot_mid": "o", "bot_right": "-"}, f"""
    {_1}  {_O}  {_X}
    {_4}  {_X}  {_6}
    {_X}  {_O}  {_9}
    """),
    ({"top_left": "o", "top_mid": "o", "top_right": "x",
      "mid_left": "x", "mid_mid": "-", "mid_right": "x",
      "bot_left": "x", "bot_mid": "o", "bot_right": "o"}, f"""
    {_O}  {_O}  {_X}
    {_X}  {_5}  {_X}
    {_X}  {_O}  {_O}
    """),
])
def test_print_board(board, expected) -> None:
    actual_output = ticTacToe.print_board(board)
    assert actual_output == expected


@pytest.mark.parametrize('player_going_first, moves, expected', [
    # PLAYER_GOING_FIRST: 1 | EXPECTED: 'player 1 won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 00 / testing basic win / rows
    ("player 1", [1, 7, 2, 8, 3], 'player 1 won'),
    ("player 1", [4, 1, 5, 2, 6], 'player 1 won'),
    ("player 1", [7, 1, 8, 2, 9], 'player 1 won'),
    # debug_inputs 03 / testing basic win / columns
    ("player 1", [1, 3, 4, 6, 7], 'player 1 won'),
    ("player 1", [2, 3, 5, 6, 8], 'player 1 won'),
    ("player 1", [3, 1, 6, 2, 9], 'player 1 won'),
    # debug_inputs 06 / testing basic win / diagonals
    ("player 1", [1, 2, 5, 4, 9], 'player 1 won'),
    ("player 1", [3, 2, 5, 4, 7], 'player 1 won'),

    # debug_inputs 08 / testing long win / rows
    ("player 1", [1, 7, 2, 8, 4, 5, 9, 6, 3], 'player 1 won'),
    ("player 1", [4, 1, 5, 2, 3, 7, 8, 9, 6], 'player 1 won'),
    ("player 1", [7, 1, 8, 2, 4, 5, 3, 6, 9], 'player 1 won'),
    # debug_inputs 11 / testing long win / columns
    ("player 1", [1, 2, 4, 5, 3, 6, 8, 9, 7], 'player 1 won'),
    ("player 1", [2, 1, 5, 3, 4, 6, 9, 7, 8], 'player 1 won'),
    ("player 1", [3, 1, 6, 4, 2, 5, 7, 8, 9], 'player 1 won'),
    # debug_inputs 14 / testing long win / diagonals
    ("player 1", [1, 3, 5, 6, 2, 7, 4, 8, 9], 'player 1 won'),
    ("player 1", [3, 1, 5, 4, 2, 8, 6, 9, 7], 'player 1 won'),

    # PLAYER_GOING_FIRST: 1 | EXPECTED: 'player 2 won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 16 / testing basic win / rows
    ("player 1", [4, 1, 5, 2, 7, 3], 'player 2 won'),
    ("player 1", [1, 4, 2, 5, 7, 6], 'player 2 won'),
    ("player 1", [4, 7, 5, 8, 1, 9], 'player 2 won'),
    # debug_inputs 19 / testing basic win / columns
    ("player 1", [2, 1, 3, 4, 5, 7], 'player 2 won'),
    ("player 1", [1, 2, 3, 5, 6, 8], 'player 2 won'),
    ("player 1", [1, 3, 2, 6, 5, 9], 'player 2 won'),
    # debug_inputs 22 / testing basic win / diagonals
    ("player 1", [9, 7, 8, 5, 6, 3], 'player 2 won'),
    ("player 1", [4, 1, 7, 5, 8, 9], 'player 2 won'),

    # debug_inputs 24 / testing long win / rows
    ("player 1", [4, 1, 5, 2, 7, 4, 8, 3], 'player 2 won'),
    ("player 1", [1, 4, 2, 5, 9, 3, 8, 6], 'player 2 won'),
    ("player 1", [1, 7, 2, 8, 4, 6, 5, 9], 'player 2 won'),
    # debug_inputs 27 / testing long win / columns
    ("player 1", [5, 1, 6, 2, 9, 4, 8, 7], 'player 2 won'),
    ("player 1", [1, 2, 3, 5, 4, 6, 9, 8], 'player 2 won'),
    ("player 1", [1, 3, 2, 6, 4, 8, 5, 9], 'player 2 won'),
    # debug_inputs 30 / testing long win / diagonals
    ("player 1", [3, 1, 4, 5, 7, 6, 8, 9], 'player 2 won'),
    ("player 1", [1, 3, 6, 4, 9, 5, 8, 7], 'player 2 won'),

    # PLAYER_GOING_FIRST: 2 | EXPECTED: 'player 2 won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 00 / testing basic win / rows
    ("player 2", [1, 7, 2, 8, 3], 'player 2 won'),
    ("player 2", [4, 1, 5, 2, 6], 'player 2 won'),
    ("player 2", [7, 1, 8, 2, 9], 'player 2 won'),
    # debug_inputs 03 / testing basic win / columns
    ("player 2", [1, 3, 4, 6, 7], 'player 2 won'),
    ("player 2", [2, 3, 5, 6, 8], 'player 2 won'),
    ("player 2", [3, 1, 6, 2, 9], 'player 2 won'),
    # debug_inputs 06 / testing basic win / diagonals
    ("player 2", [1, 2, 5, 4, 9], 'player 2 won'),
    ("player 2", [3, 2, 5, 4, 7], 'player 2 won'),

    # debug_inputs 08 / testing long win / rows
    ("player 2", [1, 7, 2, 8, 4, 5, 9, 6, 3], 'player 2 won'),
    ("player 2", [4, 1, 5, 2, 3, 7, 8, 9, 6], 'player 2 won'),
    ("player 2", [7, 1, 8, 2, 4, 5, 3, 6, 9], 'player 2 won'),
    # debug_inputs 11 / testing long win / columns
    ("player 2", [1, 2, 4, 5, 3, 6, 8, 9, 7], 'player 2 won'),
    ("player 2", [2, 1, 5, 3, 4, 6, 9, 7, 8], 'player 2 won'),
    ("player 2", [3, 1, 6, 4, 2, 5, 7, 8, 9], 'player 2 won'),
    # debug_inputs 14 / testing long win / diagonals
    ("player 2", [1, 3, 5, 6, 2, 7, 4, 8, 9], 'player 2 won'),
    ("player 2", [3, 1, 5, 4, 2, 8, 6, 9, 7], 'player 2 won'),

    # PLAYER_GOING_FIRST: 1 | EXPECTED: 'player 2 won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 16 / testing basic win / rows
    ("player 2", [4, 1, 5, 2, 7, 3], 'player 1 won'),
    ("player 2", [1, 4, 2, 5, 7, 6], 'player 1 won'),
    ("player 2", [4, 7, 5, 8, 1, 9], 'player 1 won'),
    # debug_inputs 19 / testing basic win / columns
    ("player 2", [2, 1, 3, 4, 5, 7], 'player 1 won'),
    ("player 2", [1, 2, 3, 5, 6, 8], 'player 1 won'),
    ("player 2", [1, 3, 2, 6, 5, 9], 'player 1 won'),
    # debug_inputs 22 / testing basic win / diagonals
    ("player 2", [9, 7, 8, 5, 6, 3], 'player 1 won'),
    ("player 2", [4, 1, 7, 5, 8, 9], 'player 1 won'),

    # debug_inputs 24 / testing long win / rows
    ("player 2", [4, 1, 5, 2, 7, 4, 8, 3], 'player 1 won'),
    ("player 2", [1, 4, 2, 5, 9, 3, 8, 6], 'player 1 won'),
    ("player 2", [1, 7, 2, 8, 4, 6, 5, 9], 'player 1 won'),
    # debug_inputs 27 / testing long win / columns
    ("player 2", [5, 1, 6, 2, 9, 4, 8, 7], 'player 1 won'),
    ("player 2", [1, 2, 3, 5, 4, 6, 9, 8], 'player 1 won'),
    ("player 2", [1, 3, 2, 6, 4, 8, 5, 9], 'player 1 won'),
    # debug_inputs 30 / testing long win / diagonals
    ("player 2", [3, 1, 4, 5, 7, 6, 8, 9], 'player 1 won'),
    ("player 2", [1, 3, 6, 4, 9, 5, 8, 7], 'player 1 won'),
])
def test_tic_tac_toe_no_ai(player_going_first, moves, expected) -> None:
    test_config_path = test_data_config_dir / 'test_ticTicTac.json'
    test_config = load_json_path(test_config_path, validate_json=False)
    test_input_config = {
        'tutorial': {'mode': 'test', 'input': False},
        'human_or_ai': {'mode': 'test', 'input': False},
        'player_going_first': {'mode': 'test', 'input': player_going_first},
        'picked_slot': {'mode': 'test', 'generator': (test_input for test_input in moves)}
    }
    tic = ticTacToe
    actual_output = tic.tictactoe_game(
        input_config=test_input_config,
        config=dict(test_config),
        config_path=test_config_path
    )
    with open(test_config_path, 'w') as test_config_file:
        json.dump(test_config, test_config_file, indent=2)
    assert actual_output == expected


@pytest.mark.parametrize('player_going_first, human_moves, ai_moves, expected', [
    # PLAYER_GOING_FIRST: 1 | EXPECTED: 'you won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 00 / testing basic win / rows
    ("player 1", [1, 2, 3], [7, 8], 'you won'),
    ("player 1", [4, 5, 6], [1, 2], 'you won'),
    ("player 1", [7, 8, 9], [1, 2], 'you won'),
    # debug_inputs 03 / testing basic win / columns
    ("player 1", [1, 4, 7], [3, 6], 'you won'),
    ("player 1", [2, 5, 8], [3, 6], 'you won'),
    ("player 1", [3, 6, 9], [1, 2], 'you won'),
    # debug_inputs 06 / testing basic win / diagonals
    ("player 1", [1, 5, 9], [2, 4], 'you won'),
    ("player 1", [3, 5, 7], [2, 4], 'you won'),
    # debug_inputs 08 / testing long win / rows
    ("player 1", [1, 2, 4, 9, 3], [7, 8, 5, 6], 'you won'),
    ("player 1", [4, 5, 3, 8, 6], [1, 2, 7, 9], 'you won'),
    ("player 1", [7, 8, 4, 3, 9], [1, 2, 5, 6], 'you won'),
    # debug_inputs 11 / testing long win / columns
    ("player 1", [1, 4, 3, 8, 7], [2, 5, 6, 9], 'you won'),
    ("player 1", [2, 5, 4, 9, 8], [1, 3, 6, 7], 'you won'),
    ("player 1", [3, 6, 2, 7, 9], [1, 4, 5, 8], 'you won'),
    # debug_inputs 14 / testing long win / diagonals
    ("player 1", [1, 5, 2, 4, 9], [3, 6, 7, 8], 'you won'),
    ("player 1", [3, 5, 2, 6, 7], [1, 4, 8, 9], 'you won'),
    # PLAYER_GOING_FIRST: 1 | EXPECTED: 'ai won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 16 / testing basic win / rows
    ("player 1", [4, 5, 7], [1, 2, 3], 'ai won'),
    ("player 1", [1, 2, 7], [4, 5, 6], 'ai won'),
    ("player 1", [4, 5, 1], [7, 8, 9], 'ai won'),
    # debug_inputs 19 / testing basic win / columns
    ("player 1", [2, 3, 5], [1, 4, 7], 'ai won'),
    ("player 1", [1, 3, 6], [2, 5, 8], 'ai won'),
    ("player 1", [1, 2, 5], [3, 6, 9], 'ai won'),
    # debug_inputs 22 / testing basic win / diagonals
    ("player 1", [9, 8, 6], [7, 5, 3], 'ai won'),
    ("player 1", [4, 7, 8], [1, 5, 9], 'ai won'),
    # debug_inputs 24 / testing long win / rows
    ("player 1", [4, 5, 7, 8], [1, 2, 4, 3], 'ai won'),
    ("player 1", [1, 2, 9, 8], [4, 5, 3, 6], 'ai won'),
    ("player 1", [1, 2, 4, 5], [7, 8, 6, 9], 'ai won'),
    # debug_inputs 27 / testing long win / columns
    ("player 1", [5, 6, 9, 8], [1, 2, 4, 7], 'ai won'),
    ("player 1", [1, 3, 4, 9], [2, 5, 6, 8], 'ai won'),
    ("player 1", [1, 2, 4, 5], [3, 6, 8, 9], 'ai won'),
    # debug_inputs 30 / testing long win / diagonals
    ("player 1", [3, 4, 7, 8], [1, 5, 6, 9], 'ai won'),
    ("player 1", [1, 6, 9, 8], [3, 4, 5, 7], 'ai won'),
    # PLAYER_GOING_FIRST: 2 | EXPECTED: 'ai won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 00 / testing basic win / rows
    ("player 2", [7, 8], [1, 2, 3], 'ai won'),
    ("player 2", [1, 2], [4, 5, 6], 'ai won'),
    ("player 2", [1, 2], [7, 8, 9], 'ai won'),
    # debug_inputs 03 / testing basic win / columns
    ("player 2", [3, 6], [1, 4, 7], 'ai won'),
    ("player 2", [3, 6], [2, 5, 8], 'ai won'),
    ("player 2", [1, 2], [3, 6, 9], 'ai won'),
    # debug_inputs 06 / testing basic win / diagonals
    ("player 2", [2, 4], [1, 5, 9], 'ai won'),
    ("player 2", [2, 4], [3, 5, 7], 'ai won'),
    # debug_inputs 08 / testing long win / rows
    ("player 2", [7, 8, 5, 6], [1, 2, 4, 9, 3], 'ai won'),
    ("player 2", [1, 2, 7, 9], [4, 5, 3, 8, 6], 'ai won'),
    ("player 2", [1, 2, 5, 6], [7, 8, 4, 3, 9], 'ai won'),
    # debug_inputs 11 / testing long win / columns
    ("player 2", [2, 5, 6, 9], [1, 4, 3, 8, 7], 'ai won'),
    ("player 2", [1, 3, 6, 7], [2, 5, 4, 9, 8], 'ai won'),
    ("player 2", [1, 4, 5, 8], [3, 6, 2, 7, 9], 'ai won'),
    # debug_inputs 14 / testing long win / diagonals
    ("player 2", [3, 6, 7, 8], [1, 5, 2, 4, 9], 'ai won'),
    ("player 2", [1, 4, 8, 9], [3, 5, 2, 6, 7], 'ai won'),
    # PLAYER_GOING_FIRST: 1 | EXPECTED: 'you won'| >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # debug_inputs 16 / testing basic win / rows
    ("player 2", [1, 2, 3], [4, 5, 7], 'you won'),
    ("player 2", [4, 5, 6], [1, 2, 7], 'you won'),
    ("player 2", [7, 8, 9], [4, 5, 1], 'you won'),
    # debug_inputs 19 / testing basic win / columns
    ("player 2", [1, 4, 7], [2, 3, 5], 'you won'),
    ("player 2", [2, 5, 8], [1, 3, 6], 'you won'),
    ("player 2", [3, 6, 9], [1, 2, 5], 'you won'),
    # debug_inputs 22 / testing basic win / diagonals
    ("player 2", [7, 5, 3], [9, 8, 6], 'you won'),
    ("player 2", [1, 5, 9], [4, 7, 8], 'you won'),
    # debug_inputs 24 / testing long win / rows
    ("player 2", [1, 2, 4, 3], [4, 5, 7, 8], 'you won'),
    ("player 2", [4, 5, 3, 6], [1, 2, 9, 8], 'you won'),
    ("player 2", [7, 8, 6, 9], [1, 2, 4, 5], 'you won'),
    # debug_inputs 27 / testing long win / columns
    ("player 2", [1, 2, 4, 7], [5, 6, 9, 8], 'you won'),
    ("player 2", [2, 5, 6, 8], [1, 3, 4, 9], 'you won'),
    ("player 2", [3, 6, 8, 9], [1, 2, 4, 5], 'you won'),
    # debug_inputs 30 / testing long win / diagonals
    ("player 2", [1, 5, 6, 9], [3, 4, 7, 8], 'you won'),
    ("player 2", [3, 4, 5, 7], [1, 6, 9, 8], 'you won'),
])
def test_tic_tac_toe_ai(player_going_first, human_moves, ai_moves, expected) -> None:
    test_config_path = test_data_config_dir / 'test_ticTicTac.json'
    test_config = load_json_path(test_config_path, validate_json=False)
    test_config['tutorial_asked'] = False
    test_input_config = {
        'tutorial': {'mode': 'test', 'input': True},
        'human_or_ai': {'mode': 'test', 'input': True},
        'player_going_first': {'mode': 'test', 'input': player_going_first},
        'picked_slot': {'mode': 'test', 'generator': (test_input for test_input in human_moves)},
        'ai_pick': {'mode': 'test', 'generator': (test_input for test_input in ai_moves)},
        'ai_player': {'mode': 'test', 'input': 'player 2'}
    }
    tic = ticTacToe
    actual_output = tic.tictactoe_game(
        input_config=test_input_config,
        config=dict(test_config),
        config_path=test_config_path
    )
    with open(test_config_path, 'w') as test_config_file:
        json.dump(test_config, test_config_file, indent=2)
    assert actual_output == expected
