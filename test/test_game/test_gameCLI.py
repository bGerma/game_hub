"""
tests all the functions in the gameCLI.py file
"""
import json
import pytest
from click.testing import CliRunner
try:
    from prompt_toolkit.terminal import win32_output
except ImportError:
    win32_output = None

from game_hub.games import gameCLI
from test.test_game.test_ticTacToe import test_data_root


play = """Usage: play [OPTIONS] COMMAND [ARGS]...

  Contains all games

Options:
  --help  Show this message and exit.

Commands:
  hang  Plays Hangman game
  rps   Plays Rock Paper Scissors game
  tic   Plays Tic Tac Toe game
"""


@pytest.mark.parametrize('last_game', ["1", "2", "3", "4", "5", "6", "7", "8", "9"])
def test_change_last_game(last_game) -> None:
    """
    tests the change_last_game() function
    """
    test_last_game_config_path = test_data_root / 'test_change_last_game.json'
    with open(test_last_game_config_path, 'w') as file_config:
        json.dump({"last_game": "before"}, file_config, indent=2)
    gameCLI.change_last_game(last_game, test_last_game_config_path)
    last_game_config = json.loads(test_last_game_config_path.read_text())
    assert last_game_config['last_game'] == last_game


def test_play() -> None:
    """
    tests the play command
    """
    runner = CliRunner()
    result = runner.invoke(gameCLI.play)
    assert result.exit_code == 0
    assert result.output == play


def test_hangman() -> None:
    """
    tests the play command
    """
    if win32_output is not None:
        runner = CliRunner()
        try:
            _ = runner.invoke(gameCLI.hangman, catch_exceptions=False)
            assert False
        except win32_output.NoConsoleScreenBufferError:
            assert True
    else:
        assert True


def test_rps() -> None:
    """
    tests the play command
    """
    if win32_output is not None:
        runner = CliRunner()
        try:
            _ = runner.invoke(gameCLI.rps, catch_exceptions=False)
            assert False
        except win32_output.NoConsoleScreenBufferError:
            assert True
    else:
        assert True


def test_tictactoe() -> None:
    """
    tests the play command
    """
    if win32_output is not None:
        runner = CliRunner()
        try:
            _ = runner.invoke(gameCLI.tictactoe, catch_exceptions=False)
            assert False
        except win32_output.NoConsoleScreenBufferError:
            assert True
    assert True
