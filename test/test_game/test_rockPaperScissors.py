"""
tests the rock paper scissors game
"""
import pytest
from game_hub.games import rockPaperScissors


@pytest.mark.parametrize("user_choice, computer_choice, expected", [
    # Rock Tests
    ("Rock", "rock", "we drew"),
    ("Rock", "paper", "you lose"),
    ("Rock", "scissors", "you win"),
    # Paper Tests
    ("Paper", "rock", "you win"),
    ("Paper", "paper", "we drew"),
    ("Paper", "scissors", "you lose"),
    # Scissors Tests
    ("Scissors", "rock", "you lose"),
    ("Scissors", "paper", "you win"),
    ("Scissors", "scissors", "we drew")
])
def test_check_who_won(user_choice: str, computer_choice: str, expected: str) -> None:
    """
    tests the check_who_won() function in rock paper scissors
    :param user_choice: which weapon the user chooses
    :param computer_choice: which weapon the computer chooses
    :param expected: what we expect the function to return
    """
    actual_output = rockPaperScissors.check_who_won(user_choice, computer_choice)
    assert actual_output == expected


@pytest.mark.parametrize('debug_input', [
    'Rock',
    'Paper',
    'Scissors',
])
def test_rps_game(debug_input: str) -> None:
    """
    tests the rps_game() function in rock paper scissors
    :param debug_input: the
    :return:
    """
    test_input = {
        'user_choice': {'mode': 'test', 'input': debug_input}
    }
    for _ in range(20):
        actual_output = rockPaperScissors.rps_game(test_input)
        assert actual_output in ['you lose', 'we drew', 'you win']
