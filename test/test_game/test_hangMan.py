"""
Tests hang man game
"""
import pytest

from game_hub.games.hangMan import guess_is_correct, hang_man_game, get_random_word
from test.test_game.test_ticTacToe import test_data_root


def test_get_random_word() -> None:
    """
    tests the randomise function
    """
    test_words_path = test_data_root / 'test_hang_man_words.txt'

    test_random_words_1 = []
    test_random_words_2 = []
    default_random_words_1 = []
    default_random_words_2 = []
    for _ in range(20):
        test_random_words_1.append(get_random_word(test_words_path))
        test_random_words_2.append(get_random_word(test_words_path))
        default_random_words_1.append(get_random_word())
        default_random_words_2.append(get_random_word())
    assert test_random_words_1 != test_random_words_2
    assert default_random_words_1 != default_random_words_2


@pytest.mark.parametrize('guess, expected_hidden_chars, expected_is_guess_correct', [
    ("a", ['_', '_', '_', '_'], False),
    ("e", ['_', 'e', '_', '_'], True),
    ("t", ['t', '_', '_', 't'], True)
])
def test_reveal_hidden_char(guess, expected_hidden_chars, expected_is_guess_correct) -> None:
    """
    tests the replace_space_with_guess() function
    :param guess: the user guess
    """
    list_word = 'test'
    hidden_word_chars = ['_', '_', '_', '_']
    was_guess_correct = guess_is_correct(guess, list_word, hidden_word_chars)
    assert was_guess_correct == expected_is_guess_correct
    assert hidden_word_chars == expected_hidden_chars


@pytest.mark.parametrize('debug_inputs, expected', [
    (['t', 'r', 'i', 'a', 'l'], 'Congrats, you won!'),
    (['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'], 'You failed,\nThe word was trial'),
    (['trial'], 'Congrats, you won!'),
])
def test_hang_man_game(debug_inputs: list, expected: str) -> None:
    """
    Tests the hang man game function

    :param debug_inputs: a list of letters to be inputted
    :param expected: what hang_man_game() should return
    """
    debug_input_config = {'guess': {
        'mode': 'test',
        'generator': (debug_input for debug_input in debug_inputs)
    }}
    actual_output = hang_man_game('trial', input_config=debug_input_config)
    assert actual_output == expected
